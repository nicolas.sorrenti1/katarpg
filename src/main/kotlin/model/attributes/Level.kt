package model.attributes

class Level(level: Int = 1) : Attribute(level) {
    fun calculateDifference(level: Level): Int {
        return this.amount - level.amount
    }
}