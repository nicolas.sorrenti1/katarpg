package model.attributes

interface Life {
    fun life(): Int
    fun isAlive(): Boolean
    fun decreaseBy(damage: Int)
    fun isHealeable(): Boolean
    fun healWith(healing: Int = 0)
}
