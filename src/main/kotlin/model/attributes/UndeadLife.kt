package model.attributes

class UndeadLife(private val life: Int = maxLife) : Attribute(life), Life {
    override fun life(): Int {
        return amount
    }

    override fun isAlive(): Boolean {
        return alive()
    }

    override fun decreaseBy(damage: Int) {
        this.amount = Integer.min(this.amount + damage, minLife)
    }

    override fun isHealeable(): Boolean {
        return alive()
    }

    fun alive(): Boolean {
        return minLife > amount
    }

    override fun healWith(healing: Int) {
        this.amount = minLife
    }

    companion object {
        const val maxLife = -1000
        const val minLife = 0
    }
}