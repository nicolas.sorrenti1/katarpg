package model.attributes

import kotlin.math.max
import kotlin.math.min

open class LivingLife(health: Int = maxLife) : Attribute(health), Life {
    override fun life(): Int {
        return amount
    }

    override fun isAlive(): Boolean {
        return alive()
    }

    override fun decreaseBy(damage: Int) {
        this.amount = max(this.amount - damage, minLife)
    }

    override fun isHealeable(): Boolean {
        return alive()
    }

    override fun healWith(healing: Int) {
        this.amount = min(this.amount + healing, maxLife)
    }

    internal open fun alive(): Boolean {
        return amount > minLife
    }

    companion object {
        const val maxLife = 1000
        const val minLife = 0
    }
}