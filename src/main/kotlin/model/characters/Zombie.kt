package model.characters

import model.attributes.Level
import model.attributes.Life
import model.attributes.UndeadLife

class Zombie(
    private val life: Life = UndeadLife(),
    private val level: Level = Level(),
    private val attack: Int = 100,
    private val healing: Int = 100,
) : RPGCharacter {
    override fun isAlive(): Boolean {
        return life.isAlive()
    }

    override fun health(): Int {
        return life.life()
    }

    override fun receiveAttackFrom(attacker: RPGCharacter) {
        if (!notEquals(attacker)) return;

        val levelDifference = this.level.calculateDifference(attacker.level())

        val damage = calculateDamage(attacker.attack(), levelDifference)

        this.life.decreaseBy(damage)
    }

    private fun calculateDamage(attack: Int, levelDifference: Int): Int {
        if (levelDifference >= 5) {
            return attack - (attack * 0.50).toInt()
        }
        if (levelDifference <= -5) {
            return attack + (attack * 0.50).toInt()
        }

        return attack
    }

    override fun receiveHealingFrom(healer: RPGCharacter) {
        if (canBeHealed(healer)) {
            this.life.healWith()
        }
    }

    override fun level(): Level {
        return this.level
    }

    override fun attack(): Int {
        return this.attack
    }

    override fun healing(): Int {
        return this.healing
    }

    private fun notEquals(character: RPGCharacter): Boolean {
        return this !== character
    }

    private fun canBeHealed(character: RPGCharacter): Boolean {
        return this.life.isHealeable() && notEquals(character)
    }
}