package model.characters

import model.attributes.Level
import model.attributes.LivingLife

class Knight(
    private val life: LivingLife = LivingLife(),
    private val level: Level = Level(),
    private val attack: Int = 100,
    private val healing: Int = 100,
) : RPGCharacter {
    override fun isAlive(): Boolean {
        return life.isAlive()
    }

    override fun health(): Int {
        return life.life()
    }

    override fun receiveAttackFrom(attacker: RPGCharacter) {
        if (!notEquals(attacker)) return;

        val levelDifference = this.level.calculateDifference(attacker.level())

        val damage = calculateDamage(attacker.attack(), levelDifference)

        this.life.decreaseBy(damage)
    }

    private fun calculateDamage(attack: Int, levelDifference: Int): Int {
        if (levelDifference >= 5) {
            return attack - (attack * 0.50).toInt()
        }
        if (levelDifference <= -5) {
            return attack + (attack * 0.50).toInt()
        }

        return attack
    }

    override fun receiveHealingFrom(healer: RPGCharacter) {
        if (canBeHealed(healer)) {
            this.life.healWith(healer.healing())
        }
    }

    override fun level(): Level {
        return this.level
    }

    override fun attack(): Int {
        return this.attack
    }

    override fun healing(): Int {
        return this.healing
    }

    fun canBeHealed(character: RPGCharacter): Boolean {
        return this.life.isHealeable() && !notEquals(character)
    }

    fun notEquals(character: RPGCharacter): Boolean {
        return this !== character
    }
}