package model.characters

import model.attributes.Level

interface RPGCharacter {
    fun isAlive(): Boolean
    fun health(): Int
    fun receiveAttackFrom(attacker: RPGCharacter)
    fun receiveHealingFrom(healer: RPGCharacter)
    fun level(): Level
    fun attack(): Int
    fun healing(): Int
}
