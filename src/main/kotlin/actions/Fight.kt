import model.characters.RPGCharacter

class Fight() {
    operator fun invoke(attacker: RPGCharacter, defender: RPGCharacter) {
        defender.receiveAttackFrom(attacker)
    }

}