import model.characters.RPGCharacter

class Heal() {
    operator fun invoke(healer: RPGCharacter, target: RPGCharacter) {
        target.receiveHealingFrom(healer)
    }
}