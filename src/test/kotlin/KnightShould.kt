import model.attributes.Level
import model.attributes.LivingLife
import model.characters.Knight
import org.junit.Test
import kotlin.test.assertEquals

class KnightShould {
    lateinit var rpgCharacter: Knight
    private lateinit var anotherRpgCharacter: Knight

    @Test
    fun `have less health when is attacked by another character`() {
        givenARPGCharacter()
        givenAnotherRPGCharacter()

        whenACharacterAttacks()

        val oneHundredHealthLess = 900
        thenCharacterHas(oneHundredHealthLess)
    }

    @Test
    fun `have 0 health when an attack has higher damage than remaining health`() {
        givenAnAlmostDeadCharacter()
        givenAnotherRPGCharacter()

        whenACharacterAttacks()

        val zeroHealth = 0
        thenCharacterHas(zeroHealth)
    }

    @Test
    fun `have be dead there is no remaining health`() {
        givenAnAlmostDeadCharacter()
        givenAnotherRPGCharacter()

        whenACharacterAttacks()

        thenCharacterIsDead()
    }

    @Test
    fun `not have less health when is attacked by himself`(){
        givenARPGCharacter()

        whenCharacterAttacksHimself()

        thenCharacterHasFullHealth()
    }

    @Test
    fun `not heal above maximum health when healed`() {
        givenARPGCharacter()

        whenACharacterHealsHimself()

        thenCharacterHasFullHealth()
    }

    @Test
    fun `not heal when character is dead and is healed`() {
        givenADeadCharacter()

        whenACharacterHealsHimself()

        val zeroHealth = 0
        thenCharacterHas(zeroHealth)
    }

    @Test
    fun `have same health when is healed by another character`() {
        val twoHundredHealth = 200
        givenAnDamagedCharacterWith(twoHundredHealth)
        givenAnotherRPGCharacter()

        whenAnotherCharacterHeals()

        val twoHundredHealthLeft = 200
        thenCharacterHas(twoHundredHealthLeft)
    }

    @Test
    fun `have 50% less health when attacked by other character 5 levels higher`() {
        givenARPGCharacter()
        givenAnotherLevel6RPGCharacter()

        whenACharacterAttacks()

        val eightHundredHealthLeft = 850
        thenCharacterHas(eightHundredHealthLeft)
    }

    @Test
    fun `have 50% more health when attacked by other character 5 levels lower`() {
        givenALevel6RPGCharacter()
        givenAnotherRPGCharacter()

        whenACharacterAttacks()

        val nineHundredHealthLeft = 950
        thenCharacterHas(nineHundredHealthLeft)
    }

    private fun givenARPGCharacter() {
        rpgCharacter = Knight()
    }

    private fun whenCharacterAttacksHimself() {
        rpgCharacter.receiveAttackFrom(rpgCharacter)
    }

    private fun thenCharacterHasFullHealth() {
        assertEquals(FULL_HEALTH, rpgCharacter.health())
    }

    private fun whenAnotherCharacterHeals() {
        rpgCharacter.receiveHealingFrom(anotherRpgCharacter)
    }

    private fun whenACharacterHealsHimself() {
        rpgCharacter.receiveHealingFrom(rpgCharacter)
    }

    private fun givenADeadCharacter() {
        rpgCharacter = Knight(life = LivingLife(0))
    }

    private fun givenAnotherRPGCharacter() {
        anotherRpgCharacter = Knight()
    }

    private fun givenAnAlmostDeadCharacter() {
        rpgCharacter = Knight(life = LivingLife(LOW_HEALTH))
    }

    private fun givenAnDamagedCharacterWith(health: Int) {
        rpgCharacter = Knight(life = LivingLife(health))
    }

    private fun givenAnotherLevel6RPGCharacter() {
        anotherRpgCharacter = Knight(level = Level(6))
    }

    private fun givenALevel6RPGCharacter() {
        rpgCharacter = Knight(level = Level(6))
    }

    private fun whenACharacterAttacks() {
        rpgCharacter.receiveAttackFrom(anotherRpgCharacter)
    }

    private fun thenCharacterIsDead() {
        assertEquals(false, rpgCharacter.isAlive())
    }

     open fun thenCharacterHas(health: Int) {
        assertEquals(health, rpgCharacter.health())
    }

    companion object {
        private const val LOW_HEALTH: Int = 10
        private const val FULL_HEALTH: Int = 1000
    }
}