import model.attributes.UndeadLife
import model.characters.Knight
import model.characters.Zombie
import org.junit.Test
import kotlin.test.assertEquals

class ZombieShould {
    lateinit var zombie: Zombie
    lateinit var anotherRpgCharacter: Knight

    @Test
    fun `have negative 900 health when is a zombie and receives an attack higher than remaining health`() {
        givenAZombieCharacter()
        givenAnotherRPGCharacter()

        whenACharacterAttacks()

        val negativeNineHundredHealthLeft = -900
        thenCharacterHas(negativeNineHundredHealthLeft)
    }

    @Test
    fun `have 0 health when is an almost dead zombie and receives an attack higher than remaining health`() {
        givenAnAlmostDeadZombieCharacter()
        givenAnotherRPGCharacter()

        whenACharacterAttacks()

        val zeroHealth = 0
        thenCharacterHas(zeroHealth)
    }

    @Test
    fun `be dead when is a zombie and receives an attack higher than remaining health`() {
        givenAnAlmostDeadZombieCharacter()
        givenAnotherRPGCharacter()

        whenACharacterAttacks()

        thenCharacterIsDead()
    }

    @Test
    fun `be dead when is a zombie and is healed`() {
        givenAZombieCharacter()
        givenAnotherRPGCharacter()

        whenAnotherCharacterHeals()

        thenCharacterIsDead()
    }

    @Test
    fun `have same health when is a zombie and is healed by himself`() {
        givenAZombieCharacter()

        whenACharacterHealsHimself()

        val fullZombieHealth = -1000
        thenCharacterHas(fullZombieHealth)
    }

    private fun givenAZombieCharacter() {
        zombie = Zombie(life = UndeadLife())
    }

    private fun givenAnAlmostDeadZombieCharacter() {
        zombie = Zombie(life = UndeadLife(LOW_HEALTH))
    }

    private fun whenAnotherCharacterHeals() {
        zombie.receiveHealingFrom(anotherRpgCharacter)
    }

    private fun whenACharacterHealsHimself() {
        zombie.receiveHealingFrom(zombie)
    }

    private fun givenAnotherRPGCharacter() {
        anotherRpgCharacter = Knight()
    }

    private fun whenACharacterAttacks() {
        zombie.receiveAttackFrom(anotherRpgCharacter)
    }

    private fun thenCharacterIsDead() {
        assertEquals(false, zombie.isAlive())
    }

    fun thenCharacterHas(health: Int) {
        assertEquals(health, zombie.health())
    }

    companion object {
        private const val LOW_HEALTH: Int = -10
    }
}