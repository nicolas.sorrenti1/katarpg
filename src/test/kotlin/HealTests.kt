import model.attributes.Level
import model.characters.Knight
import model.characters.RPGCharacter
import org.junit.Test
import kotlin.test.assertEquals

class HealTests {
    @Test
    fun `Should make healer character heal a target character`() {
        val rpgCharacter = Knight()
        val spyCharacter = SpyHealerRPGCharacter()
        val heal = Heal()

        heal(rpgCharacter, spyCharacter)

        assertEquals(spyCharacter.healCount, 1)
    }
}

class SpyHealerRPGCharacter(var healCount: Int = 0): RPGCharacter {
    override fun isAlive(): Boolean {
        return false;
    }

    override fun health(): Int {
        return 0
    }

    override fun receiveAttackFrom(attacker: RPGCharacter) {
    }

    override fun receiveHealingFrom(healer: RPGCharacter) {
        healCount++
    }

    override fun level(): Level {
        return Level()
    }

    override fun attack(): Int {
        return 0
    }

    override fun healing(): Int {
        return 0
    }
}
