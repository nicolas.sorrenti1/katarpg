import model.attributes.Level
import model.characters.Knight
import model.characters.RPGCharacter
import org.junit.Test
import kotlin.test.assertEquals

class FightTests {
    @Test
    fun `Should make attacker character attack the defender character`() {
        val defender = SpyAttackerRPGCharacter()
        val attacker = Knight()
        val fight = Fight()

        fight(attacker, defender)

        assertEquals(defender.attackCount, 1)
    }
}

class SpyAttackerRPGCharacter(var attackCount: Int = 0) : RPGCharacter {
    override fun isAlive(): Boolean {
        return true;
    }

    override fun health(): Int {
        return 0
    }

    override fun receiveAttackFrom(defender: RPGCharacter) {
        attackCount++
    }

    override fun receiveHealingFrom(healer: RPGCharacter) {
    }

    override fun level(): Level {
        return Level()
    }

    override fun attack(): Int {
        return 0
    }

    override fun healing(): Int {
        return 0
    }
}